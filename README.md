# ProductDashboard

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## PROJECT DESCRIPTION
As the task pointed, each product returned by `/products` endpoint are displayed in separate columns. Each column contains header with single product information (Id - product identification name,Customers - number of total customers for single product,Name - header name for column presenting product name). For each column all customers are returned as single box containing demanded information (Name, Position, Company and Quote). 

Customers list is refreshed every 30 seconds and the list is beeing replaced with new content - new set of customers returned by endpoint. 

The search functionality allows to search by name and quote. Search functionality is applied on every single key press. Moreover, the search functionality allows to remember provided search query -> so the search filter is applied on every single refresh. So user always sees only matching rows in the customers section. What's more -> the matching query highline is also implemented. So whenever user searches for the phrase -> it is being highlighted with yellow background. Hovewer there are still small bugs with this specific functionality. It's mostly associated with Angular XSS - Cross-site Scripting seacurity feature. After finding the solution to fix it -> I'll provide you with an update. 

The search funcionality is like usually based on two lists - the lists storing all data returned by api and the list containing only filtered results. This allows to eliminate additional requests to api (to collect customer list once again before it will be force updated by 30 second time limit)-> while alowing user to see previous results when removing search query. 

As I was informed - design does not play the role for now, so it was implemented to look readable. Hovewer I do not like to leave things undone, so I'll provide an update anyway to make it look PRO as well.

If there are any additional questions about the solution -> you can ask me by phone or email

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
