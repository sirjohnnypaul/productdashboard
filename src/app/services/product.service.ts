import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IProduct} from '../models/product.interface';
import {environment} from '../../environments/environment';
import {ErrorService} from './error.service';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {IProductEntity} from '../models/product-entity.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private httpClient: HttpClient, private errorService: ErrorService) {
  }

  public getProducts(): Observable<IProduct[]> {
    // return this.httpClient.get<IProduct[]>(`${environment.api_url}/products`).pipe(catchError(this.errorService.errorHandler));
    return this.httpClient.get<IProduct[]>(`${environment.api_url}/products`);
  }

  public getProduct(id: string): Observable<IProductEntity> {
    return this.httpClient.get<IProductEntity>(`${environment.api_url}/products/${id}`);
  }
}
