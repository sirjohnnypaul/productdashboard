import {HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  errorHandler(error: HttpErrorResponse) {
    return 'Unknown error';
  }
}
