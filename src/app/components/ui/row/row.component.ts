import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  constructor() { }

  ngOnInit(): void {
  }

}
