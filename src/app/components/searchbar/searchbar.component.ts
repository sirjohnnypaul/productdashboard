import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit, OnDestroy {
  form: FormGroup;
  @Output() search = new EventEmitter<string>();
  subscription: Subscription;

  constructor() {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      search: new FormControl('', Validators.required)
    });

    this.subscription = this.form.controls.search.valueChanges.subscribe(value => {
      this.search.emit(value);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
