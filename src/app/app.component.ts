import {Component, OnInit} from '@angular/core';
import {ProductService} from './services/product.service';
import {IProduct} from './models/product.interface';
import {ICustomer} from './models/customer.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  filter = null;

  public completeProductsFull: {
    product: IProduct,
    customers: ICustomer[]
  }[] = [];

  public completeProducts: {
    product: IProduct,
    customers: ICustomer[]
  }[] = [];

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.getAllData();

    setInterval(() => {
      this.completeProducts.forEach((compProd, index) => {
        this.getCustomers(compProd.product, index);
      });
    }, 30000);
  }

  private getAllData(): void {
    this.productService.getProducts().subscribe(products => {

      products.forEach(product => {
        this.productService.getProduct(product.id).subscribe(prodEntity => {
          this.completeProductsFull.push({
            product,
            customers: prodEntity.customers
          });

          this.completeProducts.push({
            product,
            customers: prodEntity.customers
          });
        });
      });
    });
  }

  private getCustomers(product: IProduct, index: number): void {
    this.productService.getProduct(product.id).subscribe(prodEntity => {
      this.completeProductsFull[index].customers = prodEntity.customers;

      if (this.filter !== null) {
        this.completeProducts[index].customers = prodEntity.customers.filter(c => c.name.includes(this.filter) || c.quote.includes(this.filter)).map(customer => {
          if (customer.name.includes(this.filter)) {
            customer.name = customer.name.replace(this.filter, `<span class="search">${this.filter}</span>`);
          }

          if (customer.quote.includes(this.filter)) {
            customer.quote = customer.quote.replace(this.filter, `<span class="search">${this.filter}</span>`);
          }

          return customer;
        });

      } else {
        this.completeProducts[index].customers = prodEntity.customers;
      }

    });
  }

  setFilter(search: string) {
    this.filter = search;

    this.completeProducts = this.completeProductsFull;

    const temp: {
      product: IProduct,
      customers: ICustomer[]
    }[] = [];


    this.completeProducts.forEach((compProd, index) => {
      temp.push({
        customers: compProd.customers.filter(c => c.name.includes(search) || c.quote.includes(search)).map(customer => {
          if (customer.name.includes(this.filter)) {
            customer.name = customer.name.replace(this.filter, `<span class="search">${this.filter}</span>`);
          }

          if (customer.quote.includes(this.filter)) {
            customer.quote = customer.quote.replace(this.filter, `<span class="search">${this.filter}</span>`);
          }

          return customer;
        }),
        product: compProd.product
      });
    });

    this.completeProducts = temp;


  }
}
