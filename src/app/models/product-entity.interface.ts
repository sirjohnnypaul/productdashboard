import {ICustomer} from './customer.interface';

export interface IProductEntity {
  id: string;
  name: string;
  customers: ICustomer[];
}
