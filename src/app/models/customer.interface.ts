import {IAddress} from './address.interface';
import {IJob} from './job.interface';

export interface ICustomer {
  id: string;
  name: string;
  dateOfBirth: string;
  address: IAddress;
  phone: string;
  username: string;
  email: string;
  avatar: string;
  job: IJob;
  quote: string;
}
