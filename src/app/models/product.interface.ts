export interface IProduct {
  id: string;
  self: string;
  name: string;
  customers: number;
}
